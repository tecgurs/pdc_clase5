<?php
/**
 * @Filename: PayloadResponse.php
 * @Description:
 * @CreatedAt: 09/09/19 19:42
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\Ajax;


class PayloadResponse extends BasicResponse
{
    /** @var string */
    private $payload;

    public function __construct(bool $success, string $message, string $payload)
    {
        parent::__construct($success, $message);
        $this->payload = $payload;
    }

    public function toArray(): array
    {
        return array_merge(parent::toArray(), ['payload' => $this->payload]);
    }
}

<?php

namespace Tg\BasicCrud;


class Defaults
{
    /** @var int */
    public $id;
    public $nombre;
    public $code;
    public $precio;
    public $unidad;

    /**
     * Defaults constructor.
     * @param string $nombre
     * @param string $code
     * @param string $precio
     * @param string $unidad
     */
    public function __construct(int $id = 0, string $nombre = '', string $code = '', string $precio = '', string $unidad = '')
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->code = $code;
        $this->precio = $precio;
        $this->unidad = $unidad;
    }
}

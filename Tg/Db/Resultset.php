<?php
/**
 * @Filename: Resultset.php
 * @Description:
 * @CreatedAt: 18/08/19 16:06
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\Db;


class Resultset
{
    private $models = [];

    /*private function __construct()
    {
    }*/

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->models;
    }

    public function pushModel(Model $model)
    {
        $this->models[] = $model;
    }

    public function count(): int
    {
        return count($this->models);
    }
}

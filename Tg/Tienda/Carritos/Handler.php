<?php

namespace Tg\Tienda\Carritos;


use Tg\Ajax\PayloadResponse;
use Tg\Tienda\Articulos\Item as ArticulosItem;
use Tg\Tienda\Exception;
use Tg\Tienda\Models\Articulos;
use Tg\Tienda\Productos\Item as ProductosItem;

class Handler
{
    /** @var Item */
    private $carritoItem;

    public function __construct()
    {
        $this->sessionId = session_id();
        $this->carritoItem = Item::findBySessionId($this->sessionId);
        //var_dump($this->carritoItem); exit;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function agregarArticulo(): PayloadResponse
    {
        $idProducto = (int) $_REQUEST['idProducto'];
        if ($idProducto < 1 ) {
            throw new Exception('El idProducto no es valido', 1001);
        }

        $productoItem = ProductosItem::readFromDb($idProducto);

        $articulo = new Articulos(
            $this->carritoItem->getId(),
            $productoItem->getId(),
            1
        );

        $articulo->save();

        $articuloItem = ArticulosItem::readFromDb(
            $this->carritoItem->getId(),
            $productoItem->getId()
        );

        return new PayloadResponse(true, 'Success', $articuloItem->generateHtmlArticulo());
    }
}

<?php
/**
 * @Filename: Articulos.php
 * @Description:
 * @CreatedAt: 09/09/19 17:16
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\Tienda\Models;


use stdClass;
use Tg\Db\Model;

class Articulos extends Model
{
    protected static $tableName = 'articulos';

    /** @var int */
    protected $carritoId;
    /** @var int */
    protected $productoId;
    /** @var int */
    protected $cantidad;

    protected $fields = ['carritoId', 'productoId', 'cantidad'];

    /**
     * Articulos constructor.
     * @param int $carritoId
     * @param int $productoId
     * @param int $cantidad
     */
    public function __construct(int $carritoId, int $productoId, int $cantidad)
    {
        $this->carritoId = $carritoId;
        $this->productoId = $productoId;
        $this->cantidad = $cantidad;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getProductoId(): int
    {
        return $this->productoId;
    }

    /**
     * @return int
     */
    public function getCantidad(): int
    {
        return $this->cantidad;
    }

    protected static function readFromStdClass(stdClass $object)
    {
        $articulo = new self(
            $object->carritoId,
            $object->productoId,
            $object->cantidad
        );
        $articulo->setFromStdClass($object);

        return $articulo;
    }
}

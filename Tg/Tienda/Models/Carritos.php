<?php

namespace Tg\Tienda\Models;


use stdClass;
use Tg\Db\Model;

class Carritos extends Model
{
    protected static $tableName = 'carritos';

    /** @var string */
    protected $session_id;

    protected $fields = ['session_id'];

    public function __construct(string $session_id)
    {
        $this->session_id = $session_id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->session_id;
    }

    protected static function readFromStdClass(stdClass $object)
    {
        $carrito = new self($object->session_id);
        $carrito->setFromStdClass($object);

        return $carrito;
    }


}

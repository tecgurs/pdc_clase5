<?php
namespace Tg\Tienda\Models;


use stdClass;
use Tg\Db\Model;

class Producto extends Model
{
    protected static $tableName = 'productos';

    /** @var string */
    protected $code;
    /** @var string */
    protected $caption;
    /** @var int */
    protected $precio_centavos;
    /** @var int */
    protected $unidadId;

    protected $fields = ['code', 'caption', 'precio_centavos', 'unidadId'];

    /**
     * Producto constructor.
     * @param string $code
     * @param string $caption
     * @param int $precio_centavos
     * @param int $unidadId
     */
    public function __construct(string $code, string $caption, int $precio_centavos, int $unidadId)
    {
        $this->code = $code;
        $this->caption = $caption;
        $this->precio_centavos = $precio_centavos;
        $this->unidadId = $unidadId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getCaption(): string
    {
        return $this->caption;
    }

    /**
     * @return int
     */
    public function getPrecioCentavos(): int
    {
        return $this->precio_centavos;
    }

    /**
     * @return int
     */
    public function getUnidadId(): int
    {
        return $this->unidadId;
    }



    protected static function readFromStdClass(stdClass $object): Producto
    {
        $producto = new self(
            $object->code,
            $object->caption,
            $object->precio_centavos,
            $object->unidadId
        );
        $producto->setFromStdClass($object);

        return $producto;
    }
}

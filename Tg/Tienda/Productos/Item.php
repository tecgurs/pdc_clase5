<?php
/**
 * @Filename: Item.php
 * @Description:
 * @CreatedAt: 19/08/19 19:42
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\Tienda\Productos;


use Tg\Db\Exception;
use Tg\Money;
use Tg\Tienda\Models\Producto;
use Tg\Tienda\Models\Unidad;

class Item
{
    /** @var int */
    private $id;
    /** @var string */
    private $code;
    /** @var string */
    private $caption;
    /** @var Money */
    private $precio;
    /** @var int */
    private $unidadId;
    /** @var string */
    private $unidadCaption;
    /** @var string */
    private $unidadCaptionPlural;

    private function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Money
     */
    public function getPrecio(): Money
    {
        return $this->precio;
    }

    public function generateImgTag(): string {
        return "<img src=\"img/{$this->code}.jpg\" alt=\"{$this->caption}\">";
    }

    public function generateHtmlCard(): string
    {
        return <<<html
<div class="col s12 m6 l4">
  <div class="card">
    <div class="card-image">
      {$this->generateImgTag()}
      <span class="card-title text-border-black">{$this->caption}</span>
    </div>
    <div class="card-content">
       Precio: {$this->precio->getString('$ ')}
    </div>
    <div class="card-action">
      <a href="#" class="carrito-agregar" data-id="{$this->id}">Agregar al carrito</a>
    </div>
  </div>
</div>
html;
    }

    /**
     * @param int $productoId
     * @return Item
     * @throws \Tg\Tienda\Exception
     */
    public static function readFromDb(int $productoId): Item
    {
        try {
            $productoModel = Producto::findFirstById($productoId);
        } catch (Exception $e) {
            throw new \Tg\Tienda\Exception("No existe producto con Id {$productoId}", 1002);
        }

        return self::readFromModel($productoModel);
    }

    public static function readFromModel(Producto $model): Item
    {
        $item = new self();
        $item->id = $model->getId();
        $item->code = $model->getCode();
        $item->caption = $model->getCaption();
        $item->precio = Money::fromCentavos($model->getPrecioCentavos());

        /** @var Unidad $unidadModel */
        $unidadModel = Unidad::findFirstById($model->getUnidadId());
        $item->unidadId = $unidadModel->getId();
        $item->unidadCaption = $unidadModel->getCaption();
        $item->unidadCaptionPlural = $unidadModel->getCaptionPlural();

        return $item;
    }
}

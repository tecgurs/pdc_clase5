<?php

use Tg\Acl\Auth;

require_once 'autoload.php';

if ( !(empty($_POST['email']) || empty($_POST['pass'])) ) {
  $auth = new Auth();
  $resultado = $auth->check($_POST['email'], $_POST['pass']);
  if ($resultado == true) {
    header("Location: {$baseUri}/index.php");
    exit;
  }
    $errorMessage = 'Usuario o password incorrectos';
}


// View or Template
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="title" content="Basic CRUD">
    <meta name="keywords" content="Basic CRUD, PHP, JS, Materialize">

    <title>Basic CRUD</title>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection">

</head>
<body>

<div class="container" role="main">
  <div class="row">
    <div class="col s12">
      <div class="card center">
        <div class="card-title">Ingresar al sitio</div>
      </div>
    </div>
    <?php if (isset($errorMessage)) { ?>
    <div class="col s12">
      <div class="card center">
        <div class="card-title red-text"><?php echo $errorMessage ?></div>
      </div>
    </div>
    <?php } ?>
    <form class="col s12" action="#" method="post">
      <div class="row">
        <div class="input-field col s12 m6">
          <input id="email" name="email" type="text">
          <label for="email">Email</label>
        </div>
        <div class="input-field col s12 m6">
          <input id="pass" name ="pass" type="password">
          <label for="pass">Password</label>
        </div>
        <div class="col s12 center">
          <button class="btn blue-grey" type="submit">Ingresar</button>
        </div>
      </div>
    </form>
  </div>
</div>

<!--  Scripts-->
<script src="js/jquery-3.3.1.js"></script>
<script src="js/materialize.js"></script>
<script src="js/prototypes.js"></script>
<script src="js/front.js"></script>
<script>

    $(document).ready(function () {
        $('select').formSelect();
    });

</script>

</body>
</html>
